package com.inspedio.entity.sprite;

import com.inspedio.entity.primitive.InsCallback;
import com.inspedio.entity.primitive.InsPoint;

/**
 * This is a Sprite object with drag & drop feature.
 * 
 * @author Hyude
 * @version 1.0
 */
public class InsDragDropSprite extends InsAnimatedSprite{
	
	protected boolean onDrag = false;
	protected boolean stillDrag = false;
	protected boolean enableDrag = true;
	protected InsPoint lastPosition;
	protected InsCallback onDragCallback;
	protected InsCallback onDropCallback;

	public InsDragDropSprite(String spritePath) {
		super(spritePath);
		this.initDragValue();
	}
	
	public InsDragDropSprite(String spritePath, int X, int Y) {
		super(spritePath, X, Y);
		this.initDragValue();
	}
	
	public InsDragDropSprite(String spritePath, int X, int Y, int Width, int Height) {
		super(spritePath, X, Y, Width, Height);
		this.initDragValue();
	}
	
	public void initDragValue(){
		this.onDrag = false;
		this.lastPosition = new InsPoint();
	}
	
	public void setDragDropCallback(InsCallback DragCallback, InsCallback DropCallback){
		this.onDragCallback = DragCallback;
		this.onDropCallback = DropCallback;
	}
	
	public void startDrag(int X, int Y){
		if(!this.onDrag){
			if(this.enableDrag){
				if(this.onDragCallback != null){
					this.onDragCallback.call();
				}
				this.stillDrag = true;
				this.onDrag = true;
			}	
		}
	}
	
	public void finishDrag(int X, int Y){
		if(this.onDrag){
			if(this.onDropCallback != null){
				this.onDropCallback.call();
			}
			this.onDrag = false;
			this.stillDrag = false;
		}
	}
	
	public void dragSprite(int X, int Y){
		if(this.onDrag){
			this.setPosition(X, Y);
			this.stillDrag = true;
		}
	}
	
	/**
	 * Use this to enable/disable Drag & Drop.
	 */
	public void enableDrag(boolean val){
		this.enableDrag = val;
	}
	
	/**
	 * Do not override this unless you want to specifically access coordinate touched
	 */
	public boolean onPointerPressed(int X, int Y) {
		if(this.isOverlap(X, Y)){
			this.startDrag(X, Y);
			return true;
		}
		return false;
	}

	public boolean onPointerReleased(int X, int Y) {
		if(this.isOverlap(X, Y)){
			this.finishDrag(X, Y);
			return true;
		}
		return false;
	}

	public boolean onPointerDragged(int X, int Y) {
		if(this.isOverlap(X, Y)){
			this.dragSprite(X, Y);
			return true;
		}
		return false;
	}
	
	public boolean onPointerHold(int X, int Y) {
		if(this.isOverlap(X, Y)){
			this.dragSprite(X, Y);
			return true;
		}
		return false;
	}
	
	public void update(){
		if(this.onDrag && !stillDrag){
			this.finishDrag(this.position.x, this.position.y);
		}
		super.update();
		this.stillDrag = false;
	}

}
