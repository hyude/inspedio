package com.inspedio.system.helper;

import com.inspedio.system.core.InsGlobal;
import com.inspedio.entity.InsBasic;
import com.inspedio.entity.actions.MoveTo;
import com.inspedio.entity.primitive.InsCallback;

/**
 * This is the helper class for camera.<br>
 * It can be used to follow an object and makes that object in the middle of camera at anytime.<br>
 * You can set Camera Boundary to limit Camera Movement (so it won't go out of world).<br>
 * 
 * @author Hyude
 * @version 1.0
 */
public class InsCamera extends InsBasic{

	protected int leftBoundary;
	protected int rightBoundary;
	protected int topBoundary;
	protected int bottomBoundary;
	
	protected int offsetX = 0;
	protected int offsetY = 0;
	
	/**
	 * The object which camera will follow and focus on. Can be null
	 */
	protected InsBasic followTarget = null;
	/**
	 * True if camera is in follow mode
	 */
	protected boolean isFollow = false;
	
	
	public InsCamera(){
		this(InsGlobal.middleX, InsGlobal.middleY, InsGlobal.screenWidth, InsGlobal.screenHeight);
	}
	
	public InsCamera(int X, int Y, int Width, int Height) {
		super(X, Y, Width, Height);
		this.setBoundary(0, InsGlobal.screenWidth, 0, InsGlobal.screenHeight);
	}
	
	/**
	 * Set Maximum Boundary of Camera (area covered with camera). Defaulted to Screen Size.
	 */
	public void setBoundary(int Left, int Right, int Top, int Bottom){
		this.leftBoundary = Left;
		this.rightBoundary = Right;
		this.topBoundary = Top;
		this.bottomBoundary = Bottom;
	}
	
	public void postUpdate(){
		this.calibratePosition();
	}
	
	public void calibratePosition(){
		if((this.isFollow) && (this.followTarget != null)){
			this.position.x = this.followTarget.getMiddleX() - offsetX;
			this.position.y = this.followTarget.getMiddleY() - offsetY;
		}
		
		this.checkBoundary();
	}
	
	protected void checkBoundary(){
		this.position.x = InsUtil.Max(InsUtil.Min(this.position.x, this.getMaxX()), this.getMinX());
		this.position.y = InsUtil.Max(InsUtil.Min(this.position.y, this.getMaxY()), this.getMinY());
	}
	
	public int getMinX()
	{
		return (this.leftBoundary + (this.size.width / 2));
	}
	
	public int getMaxX()
	{
		return (this.rightBoundary - (this.size.width / 2));
	}
	
	public int getMinY()
	{
		return (this.topBoundary + (this.size.height / 2));
	}
	
	public int getMaxY()
	{
		return (this.bottomBoundary - (this.size.height / 2));
	}
	
	/**
	 * Follow An Object (makes object always at the center of the screen).
	 */
	public void follow(InsBasic obj){
		if(obj != null){
			this.isFollow = true;
			this.followTarget = obj;
			this.calibratePosition();
		}	
	}
	
	/**
	 * Follow An Object with given offset from middle of the screen.
	 * 
	 * @param	OffsetX		Offset from Middle of Screen in X. <br>
	 * @param	OffsetY		Offset from Middle of Screen in Y. <br>
	 */
	public void follow(InsBasic obj, int OffsetX, int OffsetY){
		if(obj != null){
			this.isFollow = true;
			this.followTarget = obj;
			this.offsetX = OffsetX;
			this.offsetY = OffsetY;
			this.calibratePosition();
		}	
	}
	
	/**
	 * Unfocus Object from being followed.
	 */
	public void unfollow(){
		this.isFollow = false;
		this.followTarget = null;
	}
	
	/**
	 * Move Camera to focus into given object
	 */
	public void focus(InsBasic obj, int Duration, InsCallback onFinish){
		this.setAction(MoveTo.create(this, Duration, obj.getMiddleX(), obj.getMiddleY(), onFinish));
	}
	
	/**
	 * Call this when you want to change state.
	 */
	public void reset(){
		this.unfollow();
		this.setPosition(InsGlobal.middleX, InsGlobal.middleY);
		this.setSize(InsGlobal.screenWidth, InsGlobal.screenHeight);
	}

}
