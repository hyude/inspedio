package com.inspedio.samples;

import com.inspedio.entity.InsBasic;
import com.inspedio.entity.InsGroup;
import com.inspedio.entity.InsShape;
import com.inspedio.entity.InsState;
import com.inspedio.entity.InsText;
import com.inspedio.entity.actions.Delay;
import com.inspedio.entity.actions.MoveBy;
import com.inspedio.entity.primitive.InsCallback;
import com.inspedio.entity.ui.InsButton;
import com.inspedio.enums.CollisionType;
import com.inspedio.enums.FontSize;
import com.inspedio.enums.FontStyle;
import com.inspedio.system.core.InsGlobal;
import com.inspedio.system.helper.collision.InsCollisionCallback;

public class SampleCollisionState extends InsState{

	InsGroup bullet1;
	InsGroup bullet2;
	InsText info;
	
	int count = 0;
	
	public void create() {	
		bullet1 = new InsGroup();
		bullet2 = new InsGroup();
		
		this.add(bullet1);
		this.add(bullet2);
		
		info = new InsText("COUNT = 0", InsGlobal.middleX, 30);
		this.add(info);
		
		this.addCollisionEvent("CRASH", bullet1, bullet2, 0, CollisionType.SPHERE, new InsCollisionCallback() {
			public void call(InsBasic obj1, InsBasic obj2) {
				onCollision(obj1, obj2);
			}
		});
		
		shoot();
		
		InsButton back = new InsButton(30, InsGlobal.screenHeight - 20, 60, 40, "BACK", 0xFFFFFF);
		back.setBorder(0, 3);
		back.setCaption("BACK", 0, FontSize.LARGE, FontStyle.BOLD);
		back.setClickedCallback(new InsCallback() {
			public void call() {
				onLeftSoftKey();
			}
		});
		
		this.add(back);
	}
	
	public void onCollision(InsBasic obj1, InsBasic obj2){
		this.bullet1.remove(obj1);
		this.bullet2.remove(obj2);
		count++;
		this.refreshInfo();
	}
	
	public void shoot(){
		int delay = 50;
		
		this.createBullet(true);
		this.createBullet(false);
		
		this.info.setAction(Delay.create(delay, new InsCallback() {
			public void call() {
				shoot();
			}
		}));
	}
	
	public InsShape createBullet(boolean left){
		int speed = 100;
		int angle = InsGlobal.randomizer.nextInt(InsGlobal.screenHeight) - InsGlobal.middleY;
		final InsShape s;
		int r = 10;
		if(left){
			s = new InsShape(0, InsGlobal.middleY, r, r);
			s.setCircle(r, 0, 360);
			s.setColor(0xCC3333, true);
			s.setAction(MoveBy.create(speed, InsGlobal.screenWidth, angle, new InsCallback() {
				public void call() {
					bullet1.remove(s);
				}
			}));
			this.bullet1.add(s);
		} else {
			s = new InsShape(InsGlobal.screenWidth, InsGlobal.middleY, 20, 20);
			s.setCircle(r, 0, 360);
			s.setColor(0x3333CC, true);
			s.setAction(MoveBy.create(speed, -InsGlobal.screenWidth, angle, new InsCallback() {
				public void call() {
					bullet2.remove(s);
				}
			}));
			this.bullet2.add(s);
		}
		return s;
	}
	
	public void refreshInfo(){
		this.info.setText("COUNT = " + count);
	}
	
	public void onLeftSoftKey()
	{
		InsGlobal.switchState(new SampleButtonState(), false);
	}

}
